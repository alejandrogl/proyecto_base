#!/bin/bash

#Configuraciones de PHP, dependientes del ambiente:
sed -i "s/display_errors = Off/display_errors = $PHP_DISPLAY_ERRORS/g" /usr/local/etc/php/php.ini
sed -i "s/display_startup_errors = Off/display_startup_errors = $PHP_DISPLAY_ERRORS/g" /usr/local/etc/php/php.ini
sed -i "s/enable_dl = Off/enable_dl = $PHP_ENABLE_DL/g" /usr/local/etc/php/php.ini
sed -i "s/;error_log = syslog/error_log = $PHP_ERROR_LOG/g" /usr/local/etc/php/php.ini
sed -i "s/log_errors = On/log_errors = $PHP_LOG_ERRORS/g" /usr/local/etc/php/php.ini
sed -i "s/max_execution_time = 30/max_execution_time = $PHP_MAX_EXECUTION_TIME/g" /usr/local/etc/php/php.ini

#Archivo de configuración:
#rm -f ARCHIVO_CONFIGURACIONES (???)
#mv /tmp/ARCHIVO_CONFIGURACIONES DESTINO_FINAL
#Removiendo si existe el archivo config de bugtracker:
rm -f /var/www/html/bugtracker/config.php
#Moviendo config correcta de bugtracker en tmp al raiz del proyecto:
mv /tmp/config.php /var/www/html/phpbugtracker/config.php
#Restore mysql
mysql --host=db --user=root --password=1234 --port=3306 bugtracker < /tmp/bugtracker.sql
#mysql --host=127.0.0.1 --user=root --password=1234 --port=3306 bugtracker < /tmp/bugtracker.sql

#Dependen del archivo de configuración (???):
sed -i "s/{MYSQL_DB_HOST}/$MYSQL_DB_HOST/g" /var/www/html/phpbugtracker/config.php
sed -i "s/{MYSQL_DB_USER}/$MYSQL_DB_USER/g" /var/www/html/phpbugtracker/config.php
sed -i "s/{MYSQL_DB_PASSWORD}/$MYSQL_DB_PASSWORD/g" /var/www/html/phpbugtracker/config.php
sed -i "s/{MYSQL_DB_DATABASE}/$MYSQL_DB_DATABASE/g" /var/www/html/phpbugtracker/config.php
sed -i "s/{MYSQL_DB_PORT}/$MYSQL_DB_PORT/g" /var/www/html/phpbugtracker/config.php
#sed -i "s/{MYSQL_DB_CHARSET}/$MYSQL_DB_CHARSET/g" /var/www/html/phpbugtracker/config.php
#sed -i "s/{MYSQL_DB_COLLATION}/$MYSQL_DB_COLLATION/g" /var/www/html/phpbugtracker/config.php


a2enmod rewrite

apache2-foreground
